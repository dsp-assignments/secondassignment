import ballerina/config;
import ballerina/http;
import ballerina/kubernetes;
import ballerina/log;
import ballerina/io;

public function main()
{

io:println("HAKIM Docker and Kubernates deployment");
}

@kubernetes:Ingress {
    hostname: "ballerina.guides.io",
    name: "user-retrieval-ingress"
}

@kubernetes:Service {
    serviceType: "NodePort",
    name: "user-retrieval-service"
}

listener http:Listener userRetrievalEP = new (5248, config = {
    secureSocket: {
        keyStore: {
            path: "./security/ballerinaKeystore.p12",
            password: config:getAsString("keystore-password")
        }
    }
});

@kubernetes:ConfigMap {
    conf: "service-config.toml"
}

@kubernetes:Deployment {
    image: "ballerina.guides.io/user_retrieval_service:v1.0",
    name: "user-retrieval-service"
}

@http:ServiceConfig {
    basePath: "/users"
}

service userRetrievalService on userRetrievalEP {
    @http:ResourceConfig {
        methods: ["GET"],
        path: "/{election}"
    }
    resource function getUserInfo(http:Caller caller, http:Request request, string election) {
        string candidate = config:getAsString(string `${<@untainted>election}.candidate`);
        string position = config:getAsString(string `${<@untainted>election}.position`);

        // check if candidate exists
        json payload = { message: "Candidate not found." };
        if (candidate != "" && position != "") {
            payload = { 
                candidate: candidate, 
                position: position
            };
        }

        var responseResult = caller->respond(payload);
        if (responseResult is error) {
            log:printError("Error responding back to client.", responseResult);
        }
    }
}
