import ballerina/kafka;
import ballerina/log;
import ballerina/io;

//Kafka Consumer Client 

kafka:ConsumerConfiguration consumerConfigs = {
    bootstrapServers: "localhost:7074",
    groupId: "group-id",

    topics: ["Hakim"],
    pollingIntervalInMillis: 1000,

    keyDeserializerType: kafka:DES_INT,

    valueDeserializerType: kafka:DES_STRING,

    autoCommit: false
};

listener kafka:Consumer consumer = new (consumerConfigs);
service kafkaService on consumer {
    resource function onMessage(kafka:Consumer kafkaConsumer,
            kafka:ConsumerRecord[] records) {

        foreach var kafkaRecord in records {
            processKafkaRecord(kafkaRecord);
        }

        var commitResult = kafkaConsumer->commit();
        if (commitResult is error) {
            log:printError("System disconnected.. " +
                "Consumer offsets equivalent ", commitResult);
        }
    }
}

function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) {
    anydata value = kafkaRecord.value;
    anydata key = kafkaRecord.key;
    if (value is string) {

        if (key is int) {

            log:printInfo("Hakim Topic: " + kafkaRecord.topic);
            log:printInfo("Hakim Partition: " + kafkaRecord.partition.toString());
            log:printInfo("Hakim Key: " + key.toString());
            log:printInfo("Hakim Value: " + value);
        } else {
            log:printError("Error");
        }
    } else {
        log:printError("Check input. Error!");
    }
}

function closeRc(io:ReadableCharacterChannel eyes) {
    var result = eyes.close();
    if (result is error) {
        log:printError("An error occured.",
                        err = result);
    }
}

function closeWc(io:WritableCharacterChannel pen) {
    var result = pen.close();
    if (result is error) {
        log:printError("An error occured.",
                        err = result);
    }
}

function write(json content, string path) returns @tainted error? {

io:WritableByteChannel lid = check io:openWritableFile(path);

io:WritableCharacterChannel pencils = new (lid, "UTF8");
    var result = pencils.writeJson(content);
    closeWc(pencils);
    return result;
}

function read(string path) returns @tainted json|error {
io:ReadableByteChannel ink = check io:openReadableFile(path);

io:ReadableCharacterChannel blackink = new (ink, "UTF8");
    var result = blackink.readJson();
    closeRc(blackink);
    return result;
}
//

public function main() {

io:println("~~~~~~~~~~~~~*DSP ASSIGNMENT 2~~~~~~~~~~~~~~");
io:println("____________________________________________");
io:println("               Hakim Shop                   ");
io:println("                  VoTO                      ");
io:println("             VOTING SYSTEM                  ");
io:println("                                            ");
io:println("............................................");

io:println("VoTO system consists of two parts:");
io:println(" Part one; Hakim iMarketing needs to elect a New manager"); 

io:println("BALLOT DESCRIPTION FOR PART ONE |Hakim iMarketing New manage Election|");
io:println("The candidates are: John Doe - Graphic Designer, Eliakim Haufiku - Software Developer and James Fish - Web Developer.");
io:println("In order to be a legitimate voter, one must:");
io:println("1. Be a current company member of Hakim iMarketing");
io:println("2. Be at least 28 years of age during the voting duration");
io:println(" ");
io:println("The voting season will be from the Monday 1th April 2021 - 3rd April 2021");
io:println(" ");
io:println("Results shall be released on Friday 5th April 2021.");


io:println("______________________________________________________________________");
io:println("|                                                                    |");
io:println("|                    BALLOT FOR Hakim iMarkting                      |");
io:println("|                                                                    |");
io:println("|                                                                    |");
io:println("|             Hakim iMarketing New Managerial Position               |");
io:println("|                                                                    |");
io:println("|    Please vote for one candidate only.                             |");
io:println("|    Indicate your vote by ticking a box with a tick [√]             |");                       
io:println("|                                          _                         |");
io:println("|    Eliakim Haufiku, Software Developer  |_|                        |");
io:println("|                                          _                         |");
io:println("|    John Doe, Graphic Designer           |_|                        |");
io:println("|                                          _                         |");
io:println("|    James Fish, Web Developer            |_|                        |");
io:println("|____________________________________________________________________|");


// Members of an organisation that can take part in a voting exercise must be registered voters. 
// All information about registered voters should be persisted in the system. 
// If need be (e.g., new members joining the organisation), a voter registration campaign could be
// organised prior to the starting date of the voting exercise.

//
io:println("                     INFORMATION ABOUT REGISTERED VOTERS                       ");
io:println("       (Displayed below and also stored in json file registered.json)          ");
io:println("_______________________________________________________________________________");
io:println("|                         |     |                  |                          |");
io:println("|Fullname                 | Age | Id number        |    Position              |");
io:println("|_________________________|_____|__________________|__________________________|");
io:println("|                         |     |                  |                          |");
io:println("|Pandu Jamason            |45   |09051990          |   Clerk                  |");
io:println("|Moses Moses              |32   |07041985          |   Administrator          |");
io:println("|Hakim Letu               |40   |0432156           |   Executive Officer      |");
io:println("|Jones Jone               |19   |53332010          |   Secretary              |");
io:println("|_________________________|_____|__________________|__________________________|");

//If need be (e.g., new employees joining the organisation), a voter registration campaign could be organised prior to the starting date of the voting exercise.

int h= 0;    //If new employees join Hakim iMarketing, a voter registration campaign will be organised prior to the starting date of the voting exercises.
int m= 1;    //If new employees do NOT join Hakim iMarketing, a voter registration campaign will NOT be be organised prior to the starting date of the voting exercises.
               
 if(m==1)                    
       {
       io:println("*NO new members joined, therefore Hakim iMarketing voter registration campaign will NOT be organised prior to the starting date of the voting exercises.");
       }           
       else{            
           io:println("New members joined, therefore Hakim iMarketing voter registration campaign will be organised prior to the starting date of the voting exercises.");
       }
//              
string filePath = "company/employee/registered.json";

    json cali = {
        "**REGISTERED VOTERS": {
            "Date": "23.08.2021",
            
            "Entry 1": {
                "Fullname": "Pandu Jamasone",
             },
            
             "Entry2": {
                 "Fullname": "Moses Moses",
             },
                                      
             "Entry3": {
              "Fullname": "Hakim Letu",              
             },
			 
			 "Entry4": {
              "Fullname": "Jones Jone",              
             },
                                      
             
             "ARCHIVE VOTING RESULTS FOR Manager": {                    
             "Hakim iMarkting": "Manager election results",
             "Eliakim Haufiku": "Votes: 2",
             "John Doe" : "Votes: 1",
             "James Fish":"Votes: 1",
             "Hakim Haufiku, Software Developer": "is Hakim iMarketing new manager!"
             },      
                                      
             //                             
            "VoTo registrations compiled by": ["KIm", "Joe"]
            
            //
        }
    };
    io:println("**Writing VoTo Info*");

    var wResult = write(cali, filePath);
    if (wResult is error) {
        log:printError("An error occured! ", wResult);
    } else {
        io:println("VoTo content being processed. ");

        var rResult = read(filePath);
        if (rResult is error) {
            log:printError("Cannot process your request. ", err = rResult);
        } else {
            io:println(rResult.toJsonString());
        }
        
        log:printInfo("NO BALLOTS ARE VALID AFTER DEADLINES: Friday 5th April 2021 ");
        
    int c = 4;
    
     if (c < 4) {
        io:println("Congratulations! Eliakim Haufiku, Software Developer is Hakim iMarkting new manager! ");
    } else if (c > 0) {
        io:println("Congratulations! John Doe, Graphic Designer is Hakim iMarketing new manager!");
    } else {
        io:println("Congratulations! James Fish, Web Developer is H&M's ICE CREAM FARM© new manager!");
    }

  }
}

