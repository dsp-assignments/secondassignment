import ballerina/cache;
import ballerina/io;
import ballerina/runtime;
import ballerina/kafka;
import ballerina/log;

//producer

kafka:ProducerConfiguration producerConfiguration = {

    bootstrapServers: "localhost:7042",
    clientId: "Laptop",
    acks: "all",
    retryCount: 3,

    valueSerializerType: kafka:SER_STRING,

    keySerializerType: kafka:SER_INT
};

kafka:Producer kafkaProducer = new (producerConfiguration);


public function main() returns error? {


string message = "Ballot rejects stored here";
    var sendResult = kafkaProducer->send(message, "Hakim iMarketing", key = 1);
    if (sendResult is error) {
        log:printInfo("Kafka is not working " + sendResult.toString());
    } else {
        log:printInfo("Failed!");
    }
    var flushResult = kafkaProducer->flushRecords();
    if (flushResult is error) {
        log:printInfo("Kafka is working " + flushResult.toString());
    } else {
        log:printInfo("Successful!");
    }
    
    
    cache:Cache cache = new({
        capacity: 14,
        evictionFactor: 0.7,
        defaultMaxAgeInSeconds: 3,
        cleanupIntervalInSeconds: 3
    });

    _ = check cache.put("Hakim iMarketing **rejects**", "store");

    _ = check cache.put("Manager voting, Selected more than one canditate | No.543, Used 'x' symbol instead of 'tick' to vote | No.674, Selected more than one candidate | No.879, Selected both options | No.422, Blank ballot  ", "value2", 3600);

    if (cache.hasKey("Hakim iMarketing **rejects**")) {

        string value = <string> check cache.get("Hakim iMarketing **rejects**");
        io:println("Hakim iMarketing **rejects**: " + value);
    }

    runtime:sleep(4000);

string[] keys = cache.keys();
    io:println("Ballot number, rejection reason: $" + keys.toString() + "$");

    int size = cache.size();

    int capacity = cache.capacity();

    _ = check cache.invalidate("Manager voting--No.23, Selected more than one canditate|No.543, Used 'x' symbol instead of 'tick' to vote| No.674, Selected more than one candidate| No.879, Selected both options, No.422, Blank ballot  ");

    _ = check cache.invalidateAll();
}


