import ballerina/graphql;
import ballerina/http;


http:Listener httpListener = new(5286);

service graphql:Service /graphql on new graphql:Listener(httpListener) {

    resource function get info(int overview) returns Candidate {

        return individuals[overview];
    }
}

public type Candidate record {
    string name;   
};

Candidate candidate1 = {
    name: "Eliakim Haufiku",
};
Candidate candidate2 = {
    name: "John Doe",
};
Candidate candidate3 = {
    name: "James Fish",
};

Candidate[] individuals = [candidate1, candidate2, candidate3];
